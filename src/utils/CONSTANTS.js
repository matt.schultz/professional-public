const SECTIONS = {
	SUMMARY: "Summary",
	SKILLS: "Skills",
	EXPERIENCE: "Experience",
	EDUCATION: "Education"
};

const TOPICS = {
	MUSIC: "MUSIC",
	CODING: "CODING",
	SOCIAL: "SOCIAL",
	ALL: ""
};

const COMPANY = {
    SYNAPTIC: "Synaptic AP",
    PARCHMENT: "Parchment",
    SOCIETYLAB: "SocietyLab",
    FEDERAL_GOVT: "Centers for Medicare & Medicaid Services, US Government",
    IRC: "International Rescue Committee",
    K4P: "Kids for Peace",
    FREELANCE: "Freelance"
};

const ROLES = {
    TECH: "Primary Technologies",
    SYNA_CARES: "Synaptic Cares",
    FACILITATOR: "Facilitator",
    PROD_DESIGN: "Product Design",
    DATA: "Data Analysis",
    DEV_SCALE: "Program Development and Scaling",
    COMM_DIALOG: "Community Dialogues",
    TOP: "Theatre of the Oppressed Trainings",
    CURRIC_DEV: "Curriculum Development",
    WEBMASTER: "Webmaster"
};

const PRESENT = "Present";

const SCROLL_DUR = 333;

export {
	SECTIONS,
	TOPICS,
	COMPANY,
	ROLES,
	PRESENT,
	SCROLL_DUR
};